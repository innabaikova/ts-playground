export class Rectangle {
  constructor(public width: number = 10, public height: number) {
    this.log();
  }

  area() {
    return this.height * this.width;
  }

  log() {
    console.log(`new Rectangle was create at ${new Date()}`)
  }
}

class Square extends Rectangle {
  constructor(width: number, private color: string) {
    super(width, width);
  }

  paint(newColor: string) {
    this.color = newColor;
  }
}

// The same as

export class Rectangle2 {
    width: number
    height: number

    constructor(width: number, height: number) {
        this.width = width
        this.height = height
        this.log()
    }

    area() {
        return this.height * this.width
    }

    log() {
        console.log(`new Rectangle was create at ${new Date()}`)
    }
}

class Square2 extends Rectangle {
    private color: string
    constructor(width: number, color: string) {
        super(width, width)
        this.width = width
        this.color = color
    }

    paint(newColor: string) {
        this.color = newColor
    }
}