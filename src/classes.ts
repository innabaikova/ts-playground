class Talker {
    sayBla() {
        return 'bla bla'
    }
}

class NamedTalker extends Talker {
    constructor(private name: string) {
        super()
    }

    sayWithName() {
        console.log(`${this.name} says: "${super.sayBla()}"`)
    }

    sayWithName2() {
        console.log(`${this.name} says: "${this.sayBla()}"`)
    }
}

let talker = new NamedTalker('Inna')
talker.sayWithName()
talker.sayWithName2()