interface Validator {
    isPhoneValid(phone: string): boolean
}

class RuValidator implements Validator {
    isPhoneValid(phone: string): boolean {
        return /^((\+7|8)+([0-9]){10})$/gm.test(phone)
    }
}

class EnValidator implements Validator {
    isPhoneValid(phone: string): boolean {
        return /^((\+44)+([0-9]){10})$/gm.test(phone)
    }
}

class ValidatorsFactory {
    static create(locale: 'ru' | 'uk') {
        switch (locale) {
            case 'ru':
                return new RuValidator()
            case 'uk':
                return new EnValidator()
        }
    }
}

let data: { locale: 'ru' | 'uk'; phone: string }[] = [
    { locale: 'ru', phone: '+79998886655' },
    { locale: 'uk', phone: '+449998886655' }
]

data.forEach(({ locale, phone }) => {
    let validator = ValidatorsFactory.create(locale)
    console.log(validator.isPhoneValid(phone))
})

export default ValidatorsFactory
