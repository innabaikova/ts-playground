// The main difference between abstract class and interface is
// an implementation of stop method

abstract class Vehicle {
    abstract color: string;
    abstract drive(speed: number): void;
    stop() {
        console.log('stop')
    }
}

export class Car extends Vehicle {
    constructor(public color: string){
        super()
    }
    drive(speed: number) {
        console.log(speed)
    }
}