export function find<T>(haystack: T[], needle: T): number | never {
    let minIdx = 0
    let maxIdx = haystack.length - 1
    while (minIdx <= maxIdx) {
        let middle = Math.floor((minIdx + maxIdx) / 2)
        if (haystack[middle] === needle) {
            return middle
        }
        if (haystack[middle] > needle) {
            maxIdx = middle - 1
        } else {
            minIdx = middle + 1
        }
    }

    throw new Error('Value not in array')
}

const array = ['a', 'b', 'c', 'd', 'j', 'z']
console.log(find(array, 'n'))
