const tree = {
    page: {
        results: [
            {
                id: '1',
                children: {
                    page: {
                        results: [
                            {
                                id: '5',
                                children: {
                                    page: {
                                        results: [],
                                    },
                                },
                            },
                            {
                                id: '8',
                                children: {
                                    page: {
                                        results: [],
                                    },
                                },
                            },
                        ],
                    },
                },
            },
        ],
    },
}

const getPagesIds = (page: any, pagesIds: string[] = []) => {
    if (page.results.length === 0) {
        return pagesIds
    }
    for (let result of page.results) {
        pagesIds.push(result.id)
        getPagesIds(result.children.page, pagesIds)
    }

    return pagesIds
}

console.log(getPagesIds(tree.page))