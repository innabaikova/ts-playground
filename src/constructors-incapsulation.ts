// private constructor instead of final if you want to prevent class extension
// Good solution for singletones with static methods
class Message {
    private constructor(private content: string) {}
    static create(content: string) {
        return new Message(content)
    }
}

class Utils {
    private constructor() { }
    static notify() {
        console.log('Notification')
    }
}

let bla = Message.create('bla')
//let test = new Message('fgdg')
//Constructor of class 'Message' is private and only accessible within the class declaration.

//class Test extends Message {}
//Cannot extend a class 'Message'. Class constructor is marked as private.

// protected constructor make class to be available only for extension, not initialization
class Message2 {
    protected constructor(private content: string) {}
}

class Test extends Message2 {}
//let test = new Message2()
//Constructor of class 'Message2' is protected and only accessible within the class declaration.

export {}