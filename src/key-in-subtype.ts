type APIResponse = {
  user: {
    userId: string
    friendList: {
      count: number
      friends: {
        firstName: string
        lastName: string
      }[]
    }
  }
}

type FriendsList = APIResponse['user']['friendList']
let friends: FriendsList = { count: 1, friends: [{ firstName: 'Inna', lastName: 'Baikova'}]}
// Could be useful for GraphQL