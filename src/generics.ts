export function prop<T, U extends keyof T>(obj: T, key:U): T[U] {
  return obj[key]
}

export function keys<T extends object>(obj: T): Array<keyof T> {
  const currentKeys = [];

  for (let key in obj) {
    if (obj.hasOwnProperty(key)) currentKeys.push(key);
  }

  return currentKeys;
}

export function values<T extends object>(obj: T): Array<T[keyof T]> {
  const currentValues = [];

  for (let key in obj) {
    currentValues.push(obj[key]);
  }

  return currentValues;
}

export function append<T>(el:T , list: Array<T>) {
  return list.concat(el)
}

export function createMap<T>(list: T[]) {
  return function<U>(cb: (i: T) => U): U[] {
    const result = [];

    for (let el of list) {
      result.push(cb(el))
    }

    return result;
  }
}