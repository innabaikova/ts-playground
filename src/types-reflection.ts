type Account = {
    id: number
    isEmployee: boolean
    name: string
}

const baseAcc: Account = {
    id: 0,
    isEmployee: true,
    name: 'Inna'
}

// Make all fields optional
type OptionalAccount = {
    [K in keyof Account]?: Account[K]
}

const optAcc1: OptionalAccount = { id: 5 }
const optAcc2: Partial<Account> = { id: 8 }

// Make all fields nullable
type NullableAccount = {
    [K in keyof Account]: Account[K] | null
}

const nullAcc1: NullableAccount = { id: 1, name: null, isEmployee: true }

// Make all fields readonly
type ReadonlyAccount = {
    readonly [K in keyof Account]: Account[K]
}

const readOnlyAcc1: ReadonlyAccount = { id: 1, name: 'Inna', isEmployee: true }
const readOnlyAcc2: Readonly<Account> = {
    id: 1,
    name: 'Inna',
    isEmployee: true
}
// readOnlyAcc1.name ='vv'      Imposible
// readOnlyAcc2.name = 'fsfd'   Imposible

// Dismiss readonly (same as Account)
type Account2 = {
    -readonly [K in keyof ReadonlyAccount]: Account[K]
}

// Make all fields required
type SomeOther = {
    id: number
    name?: string
}

const noname: SomeOther = { id: 5 }
const namedOne: Required<SomeOther> = { id: 9, name: 'Bla' }

// Make subtype from fields
const someAccInfo: Pick<Account, 'id' | 'name'> = { id: 5, name: 'Inna' }
