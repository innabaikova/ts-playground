// Do not use {} or Object because it almost any - accept everything exept null or undefined

let obj: Object = 1
let obj2: {} = 'jshgjhg'
let obj3: {} = Symbol('a')

// Use object - it will not accept primitives

let obj4: object = { bla: 'bla' }
let obj5: object = [1,2,3]
let obj6: object = function () {}

// Use [key: T]: U for rest keys in objects

let obj7: { a: string, [key: string]: string } = {
    a: 'bla',
    b: 'sasd',
    c: 'sasd'
}