type CurrencyUnit = 'EUR' | 'GBP' | 'JPY' | 'USD'

type Currency = {
    unit: CurrencyUnit
    value: number
}

let Currency: {DEFAULT: CurrencyUnit, from: (value: number, unit: CurrencyUnit) => Currency} = {
    DEFAULT: 'USD',
    from(value, unit = Currency.DEFAULT) {
        return { unit, value }
    }
}

export default Currency