interface IRuValidator {
    isPhoneValid(phone: string): boolean
}

interface IEnValidator {
    isPhoneValid(phone: string): boolean
}

class RuValidator implements IRuValidator {
    isPhoneValid(phone: string): boolean {
        return /^((\+7|8)+([0-9]){10})$/gm.test(phone)
    }
}

class EnValidator implements IEnValidator {
    isPhoneValid(phone: string): boolean {
        return /^((\+44)+([0-9]){10})$/gm.test(phone)
    }
}

type Creator = {
    (locale: 'ru'): IRuValidator
    (locale: 'uk'): IEnValidator
}

class ValidatorsFactory {    
    static create: Creator = function (locale: 'ru' | 'uk') {
        switch (locale) {
            case 'ru':
                return new RuValidator()
            case 'uk':
                return new EnValidator()
        }
    }
}

const ruValidator = ValidatorsFactory.create('ru')
const enValidator = ValidatorsFactory.create('uk')
