import Currency from './companion-pattern'

let amountDue: Currency = {
    unit: 'JPY',
    value: 83733.1
}
let otherAmountDue = Currency.from(330, 'EUR')
