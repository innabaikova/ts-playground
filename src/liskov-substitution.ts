class LiskovTest {
    constructor(public name: string) {}

    sayMyName() {
        console.log(`Say my name, say my name ${this.name}`)
    }
}

class LiskovTestChild extends LiskovTest {
    constructor() {
        super('Child')
    }
    
    sing() {
        console.log('La la la')
    }
}

function testSubstitution(arg: LiskovTest) {
    arg.sayMyName()
}

testSubstitution(new LiskovTest('Inna'))
testSubstitution(new LiskovTestChild())