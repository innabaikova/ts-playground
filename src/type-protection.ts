type LegacyData = {
    id: number,
    name: string,
    message: string,
    type: 'old'
}

type Data = {
    id: number,
    title: string,
    content: string,
    type: 'new'
}

type BlaBla = {
    id: number,
    title: string,
    content: string,
    age: number,
    type: 'new'
}

type GeneralData = LegacyData | Data | BlaBla
function isLegacy(data: GeneralData): data is LegacyData {
    let determinationKey: keyof LegacyData = 'message'
    return (determinationKey in data)
}

function isBlala(data: GeneralData): data is Exclude<GeneralData, LegacyData> {
    return ('age' in data)
}

function handleData(data: LegacyData | Data) {
    if (isLegacy(data)) {
        console.log(`!${data.name.toUpperCase()}: ${data.message}`)
    } else {
        console.log(`*${data.title}: ${data.content}`)
    }
}

let legacyData: LegacyData = {id: 0, name: 'Legacy', message: `I'm legacy`, type: 'old'}
let newData: Data = {id:1, title: 'New data', content: `I'm new data`, type: 'new'}

handleData(legacyData)
handleData(newData)