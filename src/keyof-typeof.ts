const dnaToRna = {
    G: 'C',
    C: 'G',
    T: 'A',
    A: 'U'
};


// typeof dnaToRna is {G: string, C: string, T: string, A: string}
// keyof of that typeof will be 'G' | 'C' | 'T' | 'A'
type DNA = keyof typeof dnaToRna

function isValidNucleotide(value: string): value is DNA {
    return value in dnaToRna;
}

export function toRna(dna: string) {
  const nucleotides = [...dna]

  return nucleotides.map(n => {
    if (!isValidNucleotide(n)) throw new Error('Invalid input DNA.')
    return dnaToRna[n]
  }).join('')
}
