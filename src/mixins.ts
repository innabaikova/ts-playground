type DConstructor<T> = new (...args: any[]) => T

type Formattable = DConstructor<{ getDate(): Date }>

function withFormatting<TBase extends Formattable>(Base: TBase) {
    return class extends Base {
        format() {
            return this.getDate().toLocaleDateString()
        }
    }
}

class Pet {
    constructor(public name: string, private birthday: Date) {}

    getDate() {
        return this.birthday
    }
}

const FormattablePet = withFormatting(Pet)
const pet = new Pet('Rex', (new Date()))
const formattedPet = new FormattablePet('Rex', (new Date()))
console.log(formattedPet.format())