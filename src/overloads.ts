type Reserve1 = {
    (from: Date, to: Date, destination: string): string
}

let reserve1: Reserve1 = (from, to, destination) => {
    return `Trip to ${destination} has been reserved from ${from.toLocaleDateString()} to ${to.toLocaleDateString()}`
}

const from = new Date('2022-07-22')
const to = new Date('2022-08-22')
console.log(reserve1(from, to, 'Maldives'))

type Reserve2 = {
    (from: Date, to: Date, destination: string): string
    (from: Date, destination: string): string
}

let reserve2: Reserve2 = (from, toOrDestination: Date | string, destination?: string) => {
    let message = `Trip to ${destination || toOrDestination} has been reserved from ${from.toLocaleDateString()}`
    if (toOrDestination instanceof Date) {
        message += ` to ${toOrDestination.toLocaleDateString()}`
    }
    return message
}

console.log(reserve2(from, to, 'Cyprus'))
console.log(reserve2(from, 'Switzerland'))

type Reserve3 = {
    (from: Date, to: Date, destination: string): string
    (from: Date, destination: string): string
    (destination: string): string
}

let reserve3: Reserve3 = (fromOrDestination: Date | string, toOrDestination?: Date | string, destination?: string) => {
    let message = `Trip to ${destination || toOrDestination || fromOrDestination} has been reserved`
    if (typeof fromOrDestination === 'string') {
        const now = new Date()
        message += ` from ${now.toLocaleDateString()}`
    }
    if (fromOrDestination instanceof Date) {
        message += ` from ${from.toLocaleDateString()}`
    }
    if (toOrDestination instanceof Date) {
        message += ` to ${toOrDestination.toLocaleDateString()}`
    }
    return message
}

console.log(reserve3(from, to, 'Cuba'))
console.log(reserve3(from, 'Canada'))
console.log(reserve3('USA'))

type Reserve4 = {
    (destination: string, from: Date, to: Date): string
    (destination: string, from: Date): string
    (destination: string): string
}

let reserve4: Reserve4 = function(destination: string, from?: Date, to?: Date) {
    let message = `Trip to ${destination} has been reserved`
    if (from) {
        message += ` from ${from.toLocaleDateString()}`
    }
    if (to) {
        message += ` to ${to.toLocaleDateString()}`
    }
    return message
}

console.log(reserve4('Cuba', from, to,))
console.log(reserve4('Canada', from))
console.log(reserve4('USA'))