type DogInfo = {
    age: number
    breed: string
}

type DogName = 'sam' | 'rex' | 'juchka'

const CompetitionList: Record<DogName, DogInfo> = {
    sam: { age: 5, breed: 'Border Collie' },
    rex: { age: 10, breed: 'German Shepherd' },
    juchka: { age: 1, breed: 'Sweet Bun' }
}

type LocaleRules = {
    dateFormat: string
    iconSource: string
}

type Locale = 'ru' | 'en' | 'de'

const localeRules: Record<Locale, LocaleRules> = {
    ru: { dateFormat: 'dd.mm.yyyy', iconSource: 'icons/ru.png' },
    en: { dateFormat: 'm/d/yyyy', iconSource: 'icons/gb.png' },
    de: { dateFormat: 'yyyy-mm-dd', iconSource: 'icons/ge.png' }
}

export default localeRules