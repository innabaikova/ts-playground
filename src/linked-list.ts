type Node<T> = {
    value: T
    prev?: Node<T>
    next?: Node<T>
}

export class LinkedList<T> {
    private head?: Node<T>
    private tail?: Node<T>
    private size: number

    constructor() {
        this.size = 0
    }

    public push(element: T) {
        const node: Node<T> = {
            value: element,
        }

        if (this.size === 0) {
            this.head = node
            this.tail = node
        } else if (this.tail) {
            node.prev = this.tail
            this.tail.next = node
            this.tail = node
        }

        this.size++
    }

    public pop(): T | undefined {
        if (this.size === 0) {
            return
        }

        const { value } = this.tail!
        if (this.size === 1) {
            this.head = undefined
            this.tail = undefined
        } else if (this.tail) {
            this.tail = this.tail.prev!
            this.tail.next = undefined
        }

        this.size--
        return value
    }

    public shift(): T | undefined {
        if (this.size === 0) {
            return
        }

        const { value } = this.head!
        if (this.size === 1) {
            this.head = undefined
            this.tail = undefined
        } else if (this.head) {
            this.head = this.head.next!
            this.head.prev = undefined
        }

        this.size--
        return value
    }

    public unshift(element: T) {
        const node: Node<T> = {
            value: element,
        }

        if (this.size === 0) {
            this.head = node
            this.tail = node
        } else if (this.head) {
            node.next = this.head
            this.head.prev = node
            this.head = node
        }

        this.size++
    }

    public delete(element: T) {
        let node = this.head
        while (node) {
            if (node.value === element) {
                const prevNode = node.prev
                const nextNode = node.next
                if (prevNode) {
                    prevNode.next = nextNode
                } else {
                    this.head = nextNode
                }
                if (nextNode) {
                    nextNode.prev = prevNode
                } else {
                    this.tail = prevNode
                }
                this.size--
                return
            }
            node = node.next
        }
    }

    public count(): number {
        return this.size
    }
}

const list = new LinkedList<number>()
list.push(10)
list.push(20)
list.push(30)
list.delete(20)
console.log(list.count())
console.log(list.pop())
console.log(list.shift())