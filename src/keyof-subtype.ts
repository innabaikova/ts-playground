type UserType = {
    field: {id: number},
    bla: {name: string} 
}

type UserKey = keyof UserType
let keys: UserKey[] = ['field', 'bla'] //Like Object.keys() in JS

type O = {a: {b: {c: string}}}
type OKeys = keyof O // 'a'
type ODeepKeys = keyof O['a']['b'] // 'c'

// Based on article: https://medium.com/@lopatsin1990/typescript-%D0%BA%D0%BB%D1%8E%D1%87%D0%B5%D0%B2%D1%8B%D0%B5-%D1%81%D0%BB%D0%BE%D0%B2%D0%B0-keyof-%D0%B8-in-key-in-586ebe8e6460

type Rule = {
    type: string,
    prompt: string
}

type ValidationScheme<T> = {
    [K in keyof T]: {
        value: T[K],
        check: boolean,
        rules?: Rule[]
    }
}

interface LoginFormFields {
    name: string,
    email: string,
    password: string
}

type ValidationLoginFormScheme = ValidationScheme<LoginFormFields>

const validationLoginFormScheme: ValidationLoginFormScheme = {
    name: {
        value: '',
        check: false
    },
    email: {
        value: '',
        check: true,
        rules: [
            {
                type: 'email',
                prompt: 'Email should contain @'
            }
        ]
    },
    password: {
        value: '',
        check: true,
        rules: [
            {
                type: 'minLength',
                prompt: 'Password should have at least 6 characters'
            }
        ]
    }
}