const mutable = {a: 'name', b: 'bla'}

mutable.a = 'fjkhk'
console.log(mutable)

const immutable = {a: 'name', b: 'bla'} as const
// immutable.a = 'kjhkj' - Imposible because of 'as const'